/*global app, console, data, io*/
(function () {
    "use strict";
    window.app = {

        settings: {
            refresh: 1000,
            ajaxUrl: "http://server7.tezzt.nl/~theotheu/stockquotes/index.php",
            headers: [
                "Symbol",
                "Last Trade",
                "Date of Last Trade",
                "Time of Last Trade",
                "Change (in points)",
                "Open price",
                "Day's High",
                "Day's Low",
                "Volume"
            ]
        },

        stocks: [],

        series: {},

        socket: io("ws://server7.tezzt.nl:1333/"),

        parseData: function (rows) {
            var company, index;

            //Iterate over the rows and add to series
            for (index = 0; index < rows.length; index++) {

                company = rows[index].col0;

                //Check if array for company exists in series
                if (app.series[company] !== undefined) {
                    app.series[company].push(rows[index]);
                } else {
                    //company does not yet exist
                    app.series[company] = [rows[index]];
                }
            }
        },

        showData: function () {
            //return value is a dom
            var table, attributeName, quote, cell, propertyValue, company, row, lengthOfArray, headerNode, headerRow, index;

            //Create table
            table = document.createElement("table");

            //Create header
            headerRow = document.createElement('tr');

            for (index = 0; index < app.settings.headers.length; index++) {
                headerNode = document.createElement('th');
                headerNode.className = 'tableHeader';
                headerNode.innerHTML = app.settings.headers[index];
                headerRow.appendChild(headerNode);
            }

            table.appendChild(headerRow);

            //Create rows
            for (company in app.series) {
                if (app.series.hasOwnProperty(company)) {
                    lengthOfArray = app.series[company].length;
                    quote = app.series[company][lengthOfArray - 1];
                    row = document.createElement('tr');
                    row.id = app.createValidCSSNameFromCompany(company);

                    //Create cells
                    table.appendChild(row);

                    for (attributeName in quote) {
                        if (quote.hasOwnProperty(attributeName)) {
                            propertyValue = quote[attributeName];
                            cell = document.createElement('td');
                            cell.innerText = propertyValue;
                            if (quote.col4 < 0) {
                                row.className = 'loser';
                            } else if (quote.col4 > 0) {
                                row.className = 'winner';
                            }

                            row.appendChild(cell);
                        }

                    }
                }

            }
            return table;
        },

        createValidCSSNameFromCompany: function (str) {
            //Regular expression to remove characters other than A-z0-9
            return str.replace(/\W/g, "");
        },

        getRealTimeData: function () {
            app.socket.on('stockquotes', function (data) {
                app.parseData(data.query.results.row);
            });
        },

        getDataFromAJAX: function () {
            var xhr;
            xhr = new XMLHttpRequest();
            xhr.open('GET', app.settings.ajaxUrl, true);
            xhr.addEventListener("load", app.retrieveJSON);
            xhr.send();
        },


        retrieveJSON: function (e) {
            app.parseData(JSON.parse(e.target.responseText).query.results.row);
        },

        initHTML: function () {
            var container, h1Node;

            //create container
            container = document.createElement('div');
            container.id = 'container';

            app.container = container;

            //create title of application
            h1Node = document.createElement('h1');
            h1Node.innerText = "Real time Stockquote app";

            app.container.appendChild(h1Node);

            return app.container;
        },

        addToSeries: function (quote, company) {
            app.series[company].unshift(quote);
        },

        generateTestData: function () {
            var company, quote, newQuote;

            for (company in app.series) {
                if (app.series.hasOwnProperty(company)) {
                    quote = app.series[company][0];
                    newQuote = Object.create(quote);
                    newQuote.col1 = app.rnd(100, 20);                   // new value, should be calculated with rnd
                    newQuote.col2 = app.getFormattedDate();             // new date
                    newQuote.col3 = app.getFormattedTime();             // new time, including am, pm
                    newQuote.col4 = -1 + Math.floor(Math.random() * 3); // difference of price value between this quote and the previous quote

                    app.addToSeries(newQuote, company);
                    console.log(newQuote);
                }

            }
        },

        rnd: function (input, range) {
            var max = input + range,
                min = input - range;
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },

        getFormattedDate: function () {
            var date = new Date(),
                day = date.getDate(),
                month = date.getMonth() + 1, //January is 0!
                year = date.getFullYear();

            if (day < 10) {
                day = '0' + day;
            }

            if (month < 10) {
                month = '0' + month;
            }

            date = month + '/' + day + '/' + year;

            return date;
        },

        getFormattedTime: function (d) {
            //Using AM and PM
            var date = new Date(),
                hours = date.getHours(),
                minutes = date.getMinutes(),
                timeAnnotation = hours >= 12 ? 'pm' : 'am',
                strTime;

            // the hour '0' should be '12'
            hours = hours % 12;
            if (!hours) {
                hours = 12;
            }

            minutes = minutes < 10 ? '0' + minutes : minutes;

            strTime = hours + ':' + minutes + ' ' + timeAnnotation;
            return strTime;
        },

        loop: function () {
            //app.loopRealTimeData();
            app.loopGenerateTestData();
            setTimeout(app.loop, app.settings.refresh);
        },

        loopRealTimeData: function () {
            console.log('loopdieloop');
            document.querySelector('#container').removeChild(document.querySelector('table'));
            app.container.appendChild(app.showData());
        },

        loopGenerateTestData: function () {
            var table;

            app.generateTestData();

            // remove old table
            document.querySelector("#container").removeChild(document.querySelector("table"));

            // add new table
            table = app.showData();
            app.container.appendChild(table);
        },

        initRealTimeData: function () {
            app.initHTML();
            app.getRealTimeData();
            app.container.appendChild(app.showData());
            app.loop();
        },

        initGenerateTestData: function () {
            var table, container;
            container = app.initHTML();
            document.body.appendChild(container);

            // Parse initial data
            app.parseData(data.query.results.row);

            table = app.showData();
            app.container.appendChild(table);
            app.loop();
        },

        init: function () {
            //app.initRealTimeData();
            app.initGenerateTestData();
        }
    };
}());
window.addEventListener("load", app.init);