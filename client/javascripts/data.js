var data = {
    "query": {
        "count": 27,
        "created": "2015-03-09T10:29:16+01:00",
        "lang": "en-US",
        "diagnostics": {
            "publiclyCallable": "true",
            "url": {
                "execution-start-time": "1",
                "execution-stop-time": "67",
                "execution-time": "66",
                "content": "http:\/\/download.finance.yahoo.com\/d\/quotes.csv?s=ibm+orcl+bcs+stt+jpm+lgen.l+ubs+db+ben+cs+bk+kn.pa+gs+lm+ms+mtu+ntrs+gle.pa+bac+av+sdr.l+dodgx+slf+sl.l+nmr+ing+bnp.pa&f=sl1d1t1c1ohgv&e\u200c?=.csv"
            },
            "user-time": "68",
            "service-time": "66",
            "build-version": "0.2.1867"
        },
        "results": {
            "row": [
                {
                    "col0": "BCS",
                    "col1": "14.11",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-3.31",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "8900"
                },
                {
                    "col0": "STT",
                    "col1": "53.36",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-13.34",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "JPM",
                    "col1": "57.16",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "5.20",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "500"
                },
                {
                    "col0": "LGEN.L",
                    "col1": "164.98",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-31.42",
                    "col5": "196.40",
                    "col6": "197.90",
                    "col7": "195.20",
                    "col8": "2986239"
                },
                {
                    "col0": "UBS",
                    "col1": "16.84",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-3.95",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "DB",
                    "col1": "54.36",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "7.50",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "12700"
                },
                {
                    "col0": "BEN",
                    "col1": "46.27",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-4.58",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "CS",
                    "col1": "35.38",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "4.07",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "500"
                },
                {
                    "col0": "BK",
                    "col1": "29.52",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-1.23",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "KN.PA",
                    "col1": "3.11",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-0.64",
                    "col5": "3.66",
                    "col6": "3.769",
                    "col7": "3.654",
                    "col8": "3447906"
                },
                {
                    "col0": "GS",
                    "col1": "160.59",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "1.59",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "200"
                },
                {
                    "col0": "LM",
                    "col1": "34.48",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "0.68",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "MS",
                    "col1": "25.24",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-1.90",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "MTU",
                    "col1": "5.61",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-0.76",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "NTRS",
                    "col1": "44.33",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-10.40",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "GLE.PA",
                    "col1": "31.45",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-7.38",
                    "col5": "37.685",
                    "col6": "38.985",
                    "col7": "37.565",
                    "col8": "3261198"
                },
                {
                    "col0": "BAC",
                    "col1": "14.73",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "0.83",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "24750"
                },
                {
                    "col0": "AV",
                    "col1": "13.53",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "0.27",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "SDR.L",
                    "col1": "2767.66",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "156.66",
                    "col5": "2600.00",
                    "col6": "2612.00",
                    "col7": "2560.00",
                    "col8": "44957"
                },
                {
                    "col0": "DODGX",
                    "col1": "169.65",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "16.81",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "N\/A"
                },
                {
                    "col0": "SLF",
                    "col1": "30.12",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-1.92",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "SL.L",
                    "col1": "372.92",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "27.62",
                    "col5": "345.30",
                    "col6": "346.20",
                    "col7": "340.40",
                    "col8": "897480"
                },
                {
                    "col0": "NMR",
                    "col1": "8.09",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "0.31",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "0"
                },
                {
                    "col0": "ING",
                    "col1": "12.12",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "0.35",
                    "col5": "N\/A",
                    "col6": "N\/A",
                    "col7": "N\/A",
                    "col8": "4000"
                },
                {
                    "col0": "BNP.PA",
                    "col1": "42.44",
                    "col2": "03\/09\/2015",
                    "col3": "10:29am",
                    "col4": "-8.69",
                    "col5": "50.61",
                    "col6": "51.28",
                    "col7": "50.40",
                    "col8": "1576061"
                }
            ]
        }
    },
    "meta": {
        "description": "Stockquote simulator for CRIA-WT. Note that change is relative to initial value, not the previous value.",
        "parameters": ["cbfunc=<string> To force the json to be in a function. Default not set"],
        "sleep": ["sleep=<int> Program wil wait t seconds before returning. Objective is to simulate timeouts. Default set to 0"]
    }
};