#Flow of the program
<p> I have 2 flows in the program which can be set in the init function.
<br>This I have done to make it easier to switch between generateTestData and getRealTimeData.
<br>I will describe both the flows and describe de steps individually.


1. __init__
    <br> The method invoked when the script is loaded in the page. This method is used in both flows.
    <br> Within the init function I can invoke the function initGenerateTestData to start the correct flow.
2. __initGenerateTestData or initGetRealTimeData__
    <br> This is the main method for this flow. From here I call upon several methods used to create the webpage as it is supposed to be
    <br> The difference between this flow and the other is the data. As the title suggests, the data that will be loaded is fake in initGenerateTestData.
    <br> In initGetRealTimeData it is not.
3. __initHTML__
    <br> The first method in initGenerateTestData. From here i create the header of the page as well as the container that will 'contain' the whole page.
4. __parseData__
    <br> The second method in initGenerateTestData. In here I parse the data and put it into my own object app.series.
    <br> This object will contain the data that will be shown in the table on screen.
5. __showData__
    <br> In this table I actually do 2 things. I create a table for the data and after that i store the data in it.
    <br> The method returns the table. It will be saved in the table in the init function from step 1.
6. __loop__
    <br> Refreshes the data. If the previous steps are followed successfully, the website will stay in this function as long as the page is open.
    <br> This method invokes itself after a certain time so you can check the change the quotes in the table contain.


#Concepts
<p> In the code I have used some concepts that might need some explanation.
<br>Per concept I will a short description, if necessary a code example and a reference to the mandatory documentation

###Objects, including object creation and inheritance
TODO:

###websockets
TODO:

    (function() {
        window.stockquoter = {

            stocks: [],

            init: function() {
            }

        }
    }());


###XMLHttpRequest
TODO:

###AJAX
TODO:

###Callbacks
TODO:

###How to write testable code for unit-tests
TODO:
