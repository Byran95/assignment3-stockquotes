/**
 * Created by Bryan on 16-3-2015.
 */

(function() {
    var a = [1,2,3,4];

    function doRequest(a) {
        var url = "http://server7.tezzt.nl:1334/api/recursion/" + a; 	//endpont inhoud route parameter
        console.log(url);
    }

    function fn(ar) {
        var e;
        if(ar.length===0) {
            return;
        } else {
            e = ar.shift();
            console.log(e);
            doRequest(e);
            fn(ar);
        }
    }

    fn(a);
}());