/**
 * How to find the first value in a prototype given a certain property?
 * @param object
 * @param property
 * @returns {*}
 */
function fn(object, property) {
    if(o.hasOwnProperty(property)) {
        return o[property];
    } else {
        fn(o.__proto__, property)
    }
}
